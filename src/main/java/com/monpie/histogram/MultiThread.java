/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monpie.histogram;

import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Szymon
 */
public class MultiThread implements Runnable {

    LinkedList<String> asciiLines = ReadDataFromFile.lines;
    Semaphore mutex = new Semaphore(1);
    Semaphore semaphore = new Semaphore(1);
    AtomicInteger i = new AtomicInteger(-1);
    int index;
    Multiset<String> histogram = ConcurrentHashMultiset.create();

    @Override
    public void run() {

        long actual = 0;
        try {
            Calculate calculate = new Calculate();

            Thread first = new Thread(calculate, "pierwsza");
            Thread second = new Thread(calculate, "druga");
            Thread third = new Thread(calculate, "trzecia");
            Thread forth = new Thread(calculate, "czwarta");
            Thread fifth = new Thread(calculate, "piata");

            long multiStart = System.currentTimeMillis();
            first.start();
            second.start();
            third.start();
            forth.start();
            fifth.start();

            first.join();
            second.join();
            third.join();
            forth.join();
            fifth.join();

            long multiEnd = System.currentTimeMillis();
            actual = multiEnd - multiStart;

        } catch (InterruptedException ex) {
            Logger.getLogger(MultiThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (String asciiCharacter : Multisets.copyHighestCountFirst(histogram).elementSet()) {
            System.out.println(asciiCharacter + " " + histogram.count(asciiCharacter));
        }
        System.out.println("Sum of all elements: " + histogram.size());
        System.out.println("Time: " + actual);

    }

    public class Calculate implements Runnable {

        public Calculate() {
        }

        @Override
        public void run() {
            int n = i.get();
            while (true) {

                try {
                    mutex.acquire();
                    n = i.incrementAndGet();
                    mutex.release();

                } catch (InterruptedException ex) {
                    Logger.getLogger(MultiThread.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (n >= asciiLines.size()) {
                    break;
                }
                histogram.addAll(Arrays.asList(Pattern.compile("").split(asciiLines.get(n))));
            }
        }
    }
}
